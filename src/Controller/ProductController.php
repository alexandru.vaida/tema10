<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @Route("/product/{categoryName}", name="product")
     */
    public function index($categoryName)
    {
        $category;
        $entityManager = $this->getDoctrine()->getManager();
        $checkCategory = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['name' => $categoryName]);

        if (!isset($checkCategory)) {
            $category = new Category();
            $category->setName($categoryName);
        } else {
            $category = $checkCategory;
        }

        $product = new Product();
        $product->setName('Macbook');
        $product->setPrice(1499);
        $product->setDescription('MacbookPro 2019');
        $product->setCategory($category);

        $entityManager->persist($category);
        $entityManager->persist($product);
        $entityManager->flush();

        $categroies = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('product/index.html.twig', [
            'productID' => $product->getId(),
            'categoryID' => $category->getId(),
            'categoryFromProduct' => $product->getCategory()->getName(),
            'categories' => $categroies,
        ]);
    }
    /**
     * @Route("/product/{id}", name="product_show")
     */
    public function show($id){
        $product = $this->getDoctrine()
        ->getRepository(Product::class)
        ->find($id);

    if (!$product) {
        throw $this->createNotFoundException(
            'No product found for id '.$id
        );
    }

    return $this->render('product/detail.html.twig', [
        'name' => $product->getName(),
        'price' => $product->getPrice(),
        'description' => $product->getDescription(),
    ]);
    }
    /**
     * @Route("/products/all", name="product_all")
     */

     public function allProducts(){
         $repository = $this->getDoctrine()->getRepository(Product::class);
         
         $products = $repository->findAll();

        //  dump($products);
        //  die();

         return $this->render('product/all.html.twig', [
            'products' => $products
        ]);
     }

     /**
     * @Route("/product/delete/{id}", name="delete_product")
     */
    public function delete($id){

        $enitityManager = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(Product::class);
        $product = $repository->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $enitityManager->remove($product);

        $enitityManager->flush();

        return new Response('Product with id '.$id. ' was deleted');
    }

    /**
     * @Route("product/update/{id}/{name}", name="update_product")
     */

     public function update($id, $name){
        $enitityManager = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(Product::class);

        $product = $repository->find($id);

        if (!$product) {
            return $this->render('product/nice.erorr.html.twig', [
                'id' => $id
            ]);
        }

        $product->setName($name);

        $enitityManager->flush();

        return $this->render('product/detail.html.twig', [
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'description' => $product->getDescription(),
        ]); 
     }
    /**
     * @Route("/productsCategory/{id}", name="all_products")
     */
     public function productsCategory($id){
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
   
        $products = $category->getProducts();

        return $this->render('product/products.html.twig', [
            'products' => $products,
            'category' => $id,
        ]); 
     }
     /**
     * @Route("/createProduct", name="create_product")
     */
     public function new(Request $request){
         $product = new Product();

         $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

         $form = $this->createFormBuilder($product)
         ->add('name', TextType::class)
         ->add('price', IntegerType::class)
         ->add('description', TextareaType::class)
         ->add('category', ChoiceType::class, [
            'choices' => $categories
         ])
         ->add('save', SubmitType::class, ['label' => 'Create Product'])
         ->getForm();

         $form->handleRequest($request);
         $succes = 0 ;

         if($form->isSubmitted() && $form->isValid()){
             $product = $form->getData();
             $succes = 1;
             $enitityManager = $this->getDoctrine()->getManager();
             $enitityManager->persist($product);
             $enitityManager->flush();

         }  else {
             $succes = 2;
         }

         return $this->render('product/form.html.twig', [
            'form' => $form->createView(),
            'succes' => $succes,
        ]); 
     }
}
